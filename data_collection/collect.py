# %%

import math
import os
from typing import Dict, List, Tuple
from urllib.parse import urljoin

import pandas as pd
import requests
from anpy.scrutin import Scrutin
from bs4 import BeautifulSoup

URL_TEMPLATE = "http://www2.assemblee-nationale.fr/scrutins/liste/(offset)/{offset}/(legislature)/15/(type)/TOUS/(idDossier)/TOUS"

# %% Gets the urls of scrutins by the national assembly


def parse_urls(pages: int = None) -> List:
    """Collect information about the scrutin in a dictionary

    Parameters
    ----------
    pages : int, optional
        number of pages to query (100 elements per pages), by default None

    Returns
    -------
    List
        Scrutins url.
    """
    data = list()
    num = None
    nums = set()
    offset = 0
    if pages is None:
        pages = math.inf
    while offset / 100 < pages:
        url = URL_TEMPLATE.format(offset=offset)
        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, "lxml")
        should_break = False
        for line in soup.select("#listeScrutins tbody tr"):
            cells = list(line.select("td"))
            links = list(cells[2].select("a"))
            if len(links) == 2:
                link_scrutin = urljoin(url, links[1]["href"])
            else:
                link_scrutin = urljoin(url, links[0]["href"])
            num = int(cells[0].text.strip().replace("*", ""))
            if num in nums:
                should_break = True
                break
            nums.add(num)
            data.append(link_scrutin)
        if should_break:
            break
        offset += 100
    return data


urls = parse_urls(pages=1)

# %% For each deputy, load if they vted for or against a scrutin


def get_scrutins_for_against(
    url: str, data_pour=None, data_contre=None
) -> Tuple[Dict, Dict]:
    if data_pour is None:
        data_pour = dict()
    if data_contre is None:
        data_contre = dict()
    scrutin = Scrutin().download_and_build(url)
    for groupe in scrutin.groupes:
        for pour in groupe.pour:
            if pour in data_pour:
                data_pour[pour].add(scrutin.numero)
            else:
                data_pour[pour] = {scrutin.numero}
        for contre in groupe.contre:
            if contre in data_contre:
                data_contre[contre].add(scrutin.numero)
            else:
                data_contre[contre] = {scrutin.numero}
    return data_pour, data_contre


pour = dict()
contre = dict()
for i in range(100):
    if i % 10 == 0:
        print("Step", i)
    pour, contre = get_scrutins_for_against(urls[i], pour, contre)


# %% Export the results in two csv files

if not os.path.exists("../data"):
    os.makedirs("../data")

pour_pandas = pd.concat([pd.DataFrame(v, columns=[k]) for k, v in pour.items()], axis=1)
pour_pandas.to_csv("../data/pour.csv")

contre_pandas = pd.concat(
    [pd.DataFrame(v, columns=[k]) for k, v in contre.items()], axis=1
)
contre_pandas.to_csv("../data/contre.csv")
