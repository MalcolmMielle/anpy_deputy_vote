# %%
# import numpy as np
from typing import List
import numpy as np
import tensorflow as tf
import tensorflow_datasets as tfds
import importlib
import sys
from loss_song import LossClusteringSong
from song_network import Autoencoder

# rng = np.random.default_rng(12345)

# %% Train dataset
(ds_train_original, ds_test_original), ds_info = tfds.load(
    "mnist",
    split=["train", "test"],
    shuffle_files=True,
    as_supervised=True,
    with_info=True,
)

# %%
batch_size = 2014


def normalize_img(image, label):
    """Normalizes images: `uint8` -> `float32`."""
    return tf.cast(image, tf.float32) / 255.0, label


ds_train_original = ds_train_original.map(
    normalize_img, num_parallel_calls=tf.data.AUTOTUNE
)
ds_train = ds_train_original.cache()
ds_train = ds_train.shuffle(
    ds_info.splits["train"].num_examples, reshuffle_each_iteration=False
)
# replace labels by image itself for decoder/encoder
ds_train = ds_train.batch(batch_size).map(lambda x, _: (x, x))
ds_train = ds_train.prefetch(tf.data.AUTOTUNE)


# %% test dataset

ds_test_original = ds_test_original.map(
    normalize_img, num_parallel_calls=tf.data.AUTOTUNE
)
ds_test = ds_test_original.batch(batch_size).map(lambda x, _: (x, x))
ds_test = ds_test.cache()
ds_test = ds_test.prefetch(tf.data.AUTOTUNE)


# %% Build model


importlib.reload(sys.modules["loss_song"])
from loss_song import LossClusteringSong, Centroid
from song_network import Autoencoder

# %%

latent_dim = 10

autoencoder = Autoencoder(latent_dim, (28, 28, 1), 0)
# loss = LossClusteringSong(autoencoder.encoder, 0.1, latent_dim, batch_size=batch_size)

autoencoder.compile(
    optimizer="adam",
    # loss=loss,
    run_eagerly=True,
)

# autoencoder.build((batch_size, 28, 28, 1))
# autoencoder.summary()
# autoencoder.encoder.summary()
# autoencoder.decoder.summary()

csv_logger = tf.keras.callbacks.CSVLogger("training.log", append=True)

iterator = iter(ds_train)
el = iterator.get_next()[0]

# %% Plots
import matplotlib.pyplot as plt


def plot(el: tf.Tensor):
    encoded_img = autoencoder.encoder(el)
    decoded_img = autoencoder.decoder(encoded_img)
    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(3, n, i + 1)
        plt.imshow(el[i])
        plt.title("original")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display reconstruction
        ax = plt.subplot(3, n, i + 1 + n)
        plt.imshow(decoded_img[i])
        plt.title("reconstructed")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()


def plot_one(el: tf.Tensor):
    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(3, n, i + 1)
        plt.imshow(el[i])
        plt.title("original")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()


def plot_centroids(centroids: List[np.ndarray]):
    centroids_array = np.array(centroids)
    decoded_c = autoencoder.decoder(centroids_array)
    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # # display cluster
        ax = plt.subplot(3, n, i + 1 + n + n)
        plt.imshow(decoded_c[i])
        plt.title("clusters")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()


# %% Simpified pre-trained - author uses RBM.


history = autoencoder.fit(
    ds_train,
    epochs=10,
    shuffle=True,
    callbacks=[csv_logger],
    validation_data=ds_test,
)
plot(el)
plot_centroids(autoencoder.loss_song.centroids.centroids)

# %%

for i in range(10):
    # iterator = iter(ds_train)
    # el = iterator.get_next()[0]
    autoencoder.loss_song.init_centroids()
    autoencoder.loss_song.sum_centroids(el, 0)
    autoencoder.loss_song.update_centroids()
    autoencoder.loss_song.update_assignments(el, 0)

    print("AF", autoencoder.loss_song.centroids.associated_features)
    plot(el)
    plot_centroids(autoencoder.loss_song.centroids.centroids)

# %%

autoencoder.loss_song.lambda_t = 0.1

n = 1
for i in range(n):

    history = autoencoder.fit(
        ds_train,
        epochs=1,
        shuffle=True,
        callbacks=[csv_logger],
        validation_data=ds_test,
    )
    autoencoder.step = 0

    # %%

    # Update centroids
    # iterator = iter(ds_train)
    autoencoder.loss_song.init_centroids()
    for count_sum, element in enumerate(ds_train):
        if count_sum % 10 == 0:
            print("Step", count_sum)
        autoencoder.loss_song.sum_centroids(element[0], count_sum)
        count_sum += 1
    print("AF", autoencoder.loss_song.centroids.associated_features)

    for count_update, element in enumerate(ds_train):
        if count_update % 10 == 0:
            print("Step assignement", count_update)
        autoencoder.loss_song.update_assignments(element[0], count_update)
        count_update += 1
    plot(el)
    plot_centroids(autoencoder.loss_song.centroids.centroids)


# %% Test the method

encoded_imgs = autoencoder.encoder.predict(ds_test)
decoded_imgs = autoencoder.decoder.predict(encoded_imgs)


# %% Plot results

iterator = iter(ds_test)

el = iterator.get_next()[0]


# %%
