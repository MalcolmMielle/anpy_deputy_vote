# %% Imports

from typing import Dict, List, Tuple

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split

from loss_song import LossClusteringSong
from song_network import Autoencoder

rng = np.random.default_rng(12345)

# %% Load data from csv

pour_data = pd.read_csv("../data/pour.csv", index_col=0)

# %% Encode
# We map each scrutins unique number to a number between [0, inf] to be able to create one-hot encodings for the training and test sets.


def filter_and_encode(serie: pd.Series, encoder: Dict) -> List:
    vals = serie.dropna().values
    return [encoder[x] for x in vals]


def encode_data(dataframe: pd.DataFrame) -> Tuple[pd.DataFrame, Dict]:

    dataframe_copy = dataframe.copy()
    # get unique elements
    unique_vals = np.unique(np.array(dataframe_copy))
    # Removes the NaN from the array
    unique_vals = unique_vals[np.logical_not(np.isnan(unique_vals))]

    # create a mapping between the values and index from [0 to inf]
    codes, uniques = pd.factorize(unique_vals)
    max_code = max(codes)
    encoder = dict(zip(uniques, codes))
    encoder[np.nan] = np.nan

    # Since the column have unequal number of values, we create a dictionnary out of the dataframe to remove the nan
    values_per_column = {
        c: filter_and_encode(dataframe_copy[c], encoder) for c in dataframe_copy.columns
    }

    # Create a numpy array representing the encoding where
    # for a scrutin i, if the deputy has voted for it, the value
    # at index i is 1, otherwise it is 0.
    a = np.zeros(shape=(max_code + 1, len(dataframe_copy.columns)))
    for count, key in enumerate(values_per_column):
        for value in values_per_column[key]:
            a[int(value)][count] = 1

    return pd.DataFrame(a, columns=dataframe_copy.columns), encoder


encoded_data, encoder = encode_data(dataframe=pour_data)

# %% Make train and test datasets

# Row are the deputies, and cols are the votes/features.
features = encoded_data.to_numpy().transpose()

data_train, data_test, labels_train, labels_test = train_test_split(
    features, features, test_size=0.20, random_state=42
)

data_train = np.array(data_train)
data_test = np.array(data_test)

# %% Batch generator


class batch_generator(tf.keras.utils.Sequence):
    """The features and labels are the same since we are training a auto-encoder."""

    def __init__(self, features: np.ndarray, batch_size: int):
        self.features = features
        self.batch_size = batch_size

    def __len__(self):
        return int(np.ceil(len(self.features) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.features[idx * self.batch_size : (idx + 1) * self.batch_size]
        return batch_x, batch_x


batch_size = 10
training_batch_generator = batch_generator(data_train, batch_size)
test_batch_generator = batch_generator(data_test, batch_size)


# %% Auto-Encoder

latent_dim = 10

autoencoder = Autoencoder(latent_dim)
autoencoder.compile(
    optimizer="adam",
    loss=LossClusteringSong(
        autoencoder.encoder, 0.1, latent_dim, batch_size=batch_size
    ),
    run_eagerly=True,
)


# %%
csv_logger = tf.keras.callbacks.CSVLogger("training.log", append=True)

for i in range(10):
    print(i)

    history = autoencoder.fit(
        x=test_batch_generator,
        # x=items[0],
        # y=items[1],
        epochs=1,
        shuffle=True,
        validation_data=test_batch_generator,
        callbacks=[csv_logger],
    )

    idx = rng.integers(low=0, high=len(training_batch_generator), size=1)
    items = training_batch_generator[idx[0]]
    autoencoder.loss.update(items[0])

# %%

import matplotlib.pyplot as plt

# Plot history: MAE
plt.plot(history.history["loss"], label="MAE (training data)")
plt.plot(history.history["val_loss"], label="MAE (validation data)")
plt.title("MAE for Chennai Reservoir Levels")
plt.ylabel("MAE value")
plt.xlabel("No. epoch")
plt.legend(loc="upper left")
plt.show()

# %%
